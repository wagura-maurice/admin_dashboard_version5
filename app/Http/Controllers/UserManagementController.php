<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserManagementController extends Controller
{
    // view
    public function index()
    {
        $data = DB::table('users')->get();
        return view('usermanagement.userrole',compact('data'));
    }
}
