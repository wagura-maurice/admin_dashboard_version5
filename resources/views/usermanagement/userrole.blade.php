@extends('layouts.master')
@section('menu')
    <!-- [ Layout sidenav ] Start -->
    <div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-dark">
        <!-- Brand demo (see assets/css/demo/demo.css) -->
        <div class="app-brand demo">
            <span class="app-brand-logo demo">
                <img src="{{URL::to('assets/img/logo-thumb.png')}}" alt="Brand Logo" class="img-fluid">
            </span>
            <a href="index.html" class="app-brand-text demo sidenav-text font-weight-normal ml-2">SOENGSOUY</a>
            <a href="javascript:" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
                <i class="ion ion-md-menu align-middle"></i>
            </a>
        </div>
        <div class="sidenav-divider mt-0"></div>

        <!-- Links -->
        <ul class="sidenav-inner py-1">

            <!-- Dashboards -->
            <li class="sidenav-item active">
                <a href="{{ route('home') }}" class="sidenav-link">
                    <i class="sidenav-icon feather icon-home"></i>
                    <div>Dashboards</div>
                    <div class="pl-1 ml-auto">
                        <div class="badge badge-danger">Admin</div>
                    </div>
                </a>
            </li>

            <!-- Layouts -->
            <li class="sidenav-divider mb-1"></li>
            <li class="sidenav-header small font-weight-semibold">Menu</li>

            <!-- UI elements -->
            <li class="sidenav-item active open">
                <a href="javascript:" class="sidenav-link sidenav-toggle">
                    <i class="sidenav-icon fa fa-user"></i>
                    <div>User Management</div>
                </a>
                <ul class="sidenav-menu">
                    <li class="sidenav-item active">
                        <a href="{{ route('role/user/view') }}" class="sidenav-link">
                            <div>User Role</div>
                        </a>
                    </li>
                
                    <li class="sidenav-item">
                        <a href="bui_progress.html" class="sidenav-link">
                            <div>Maintanain user</div>
                        </a>
                    </li>

                </ul>
            </li>

            <!-- Forms & Tables -->
            <li class="sidenav-divider mb-1"></li>
            <li class="sidenav-header small font-weight-semibold">Forms</li>
            <li class="sidenav-item">
                <a href="javascript:" class="sidenav-link sidenav-toggle">
                    <i class="sidenav-icon feather icon-clipboard"></i>
                    <div>Forms</div>
                </a>
                <ul class="sidenav-menu">
                    <li class="sidenav-item">
                        <a href="forms_layouts.html" class="sidenav-link">
                            <div>Layouts and elements</div>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="forms_input-groups.html" class="sidenav-link">
                            <div>Input groups</div>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="sidenav-item">
                <a href="tables_bootstrap.html" class="sidenav-link">
                    <i class="sidenav-icon feather icon-grid"></i>
                    <div>Tables</div>
                </a>
            </li>
        </ul>
    </div>
    <!-- [ Layout sidenav ] End -->
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.bootstrap4.min.css">
    <style>
        /* close icon */
        .close:focus, .close:hover {
            color: rgb(255, 0, 0) ;
            text-decoration: none;
            opacity: .75;
            outline: none !important;
        }
        .close {
            font-size: 45px !important;
            margin-top: 5px !important;
        }
    </style>

@endsection
@section('content')
<!-- [ Layout content ] Start -->
<div class="layout-content">
    <!-- [ content ] Start -->
    <div class="container-fluid flex-grow-1 container-p-y">
        <h4 class="font-weight-bold py-3 mb-0">User Management / User Role</h4>
        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="feather icon-home"></i></a></li>
                <li class="breadcrumb-item">User</li>
                <li class="breadcrumb-item active">User Role</li>
            </ol>
        </div>

        <div class="card mb-4">
            <h6 class="card-header"><i class="feather icon-user"></i> User Infomation</h6>
            <div class="card-body">
                <form>
                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Full Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="Enter full name">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Email Address</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" placeholder="Enter email">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Phone Number</label>
                        <div class="col-sm-10">
                            <input type="tel" class="form-control" placeholder="Enter phone number">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Group ID</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="Enter group id">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Role Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="Enter role name">
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right"></label>
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card mb-4">
            <h6 class="card-header"><i class="feather icon-user"></i> List User</h6>
            <div class="card-body">
                <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Full Name</th>
                            <th>Email Address</th>
                            <th>Phone Number</th>
                            <th>Group ID</th>
                            <th>Role Name</th>
                            <th>Modify</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td class="id">{{ $item->id }}</td>
                                <td class="name">{{ $item->name }}</td>
                                <td class="email">{{ $item->email }}</td>
                                <td class="phone_number">{{ $item->phone_number }}</td>
                                <td class="group_id">{{ $item->group_id }}</td>
                                <td class="role_name">{{ $item->role_name }}</td>
                                <td class="text-center">
                                    <a class="m-r-15 text-muted userView" data-toggle="modal" data-id="'.$item->id.'" data-target="#UserView">
                                        <i class="fa fa-eye" style="color: #0ecf48;"></i>
                                    </a>  
                                    <a class="m-r-15 text-muted userEdit" data-toggle="modal" data-idUpdate="'.$item->id.'" data-target="#UserUpdate">
                                        <i class="fa fa-edit" style="color: #2196f3;"></i>
                                    </a>
                                    <a href="delete/{{ $item->id }}" onclick="return confirm('Are you sure to want to delete it?')"><i class="fa fa-trash" style="color: red;"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- [ content ] End -->

    <!-- [ Layout footer ] Start -->
    <nav class="layout-footer footer bg-white">
        <div class="container-fluid d-flex flex-wrap justify-content-between text-center container-p-x pb-3">
            <div class="pt-3">
                <span class="footer-text font-weight-semibold">&copy; <a href="https://SoengSouy.com" class="footer-link" target="_blank">SoengSouy</a></span>
            </div>
            <div>
                <a href="javascript:" class="footer-link pt-3">About Us</a>
                <a href="javascript:" class="footer-link pt-3 ml-4">Help</a>
                <a href="javascript:" class="footer-link pt-3 ml-4">Contact</a>
                <a href="javascript:" class="footer-link pt-3 ml-4">Terms &amp; Conditions</a>
            </div>
        </div>
    </nav>
    <!-- [ Layout footer ] End -->
</div>

<!-- Modal View-->
<div class="modal fade" id="UserView" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Detial</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="registration" action="" method = "post"><!-- form add -->
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="v_id" name="id" value=""/>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Full Name</label>
                            <div class="col-sm-9">
                                <input type="text" id="v_name"name="name" class="form-control" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email Address</label>
                            <div class="col-sm-9">
                                <input type="text" id="v_email"name="email" class="form-control" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Phone Number</label>
                            <div class="col-sm-9">
                                <input type="tel" id="v_phone_number"name="mobile" class="form-control" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Group ID</label>
                            <div class="col-sm-9">
                                <input type="text" id="v_groupid"name="groupid" class="form-control" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Role Name</label>
                            <div class="col-sm-9">
                                <input type="text" id="v_role_name"name="role_name" class="form-control" value=""/>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- form add end -->
            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="icofont icofont-eye-alt"></i>Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal View-->

<!-- Modal Update-->
<div class="modal fade" id="UserUpdate" tabindex="-1" aria-labelledby="update" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="update">Update</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="registration" action="" method = "post"><!-- form add -->
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Full Name</label>
                            <div class="col-sm-9">
                                <input type="text" id="e_name"name="name" class="form-control" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email Address</label>
                            <div class="col-sm-9">
                                <input type="text" id="e_email"name="email" class="form-control" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Phone Number</label>
                            <div class="col-sm-9">
                                <input type="tel" id="e_phone_number"name="phone_number" class="form-control" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Group ID</label>
                            <div class="col-sm-9">
                                <input type="text" id="e_groupid"name="groupid" class="form-control" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Role Name</label>
                            <div class="col-sm-9">
                                <input type="text" id="e_role_name"name="role_name" class="form-control" value="" />
                            </div>
                        </div>
                    </div>
                </form>
                <!-- form add end -->
            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="icofont icofont-eye-alt"></i>Close</button>
                    <button type="submit" id=""name="" class="btn btn-success  waves-light"><i class="icofont icofont-check-circled"></i>Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Update-->


<!-- [ Layout content ] Start -->
@section('script')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js"></script>

{{-- view js --}}
<script>
    $(document).on('click','.userView',function()
    {
        var _this = $(this).parents('tr');
        $('#v_id').val(_this.find('.id').text());
        $('#v_name').val(_this.find('.name').text());
        $('#v_email').val(_this.find('.email').text());
        $('#v_phone_number').val(_this.find('.phone_number').text());
        $('#v_group_id').val(_this.find('.group_id').text());
        $('#v_role_name').val(_this.find('.role_name').text());
    });
</script>

<script>
    $(document).ready(function()
    {
        $('#example').DataTable( {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal( {
                        header: function ( row ) {
                            var data = row.data();
                            return 'Details for '+data[0]+' '+data[1];
                        }
                    } ),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                        tableClass: 'table'
                    } )
                }
            }
        } );
    } );
</script> 

@endsection
@endsection
